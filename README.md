# Email Box

[Available on bitbucket](https://bitbucket.org/fmonniot/emailbox)

## Usage

Before being able to use this application, we have to "install" it (meaning gradle will create script and jar):

```
./gradlew consumer:install
./gradlew producer:install
```

We can now use this application ```./produce [destination [newsgroup]]``` where:

* ```destination``` is the name of the mailbox to which we will send a message
* ```newsgroup``` if given the message will be sent to a newsgroup (publish)

And ```./consume [mailbox]``` where:

* ```mailbox``` is the name of the mailbox to which we will send a message

Note: if you don't have the symlinks in the archive you can clone this repo with the following command
```git clone git@bitbucket.org:fmonniot/emailbox.git```
