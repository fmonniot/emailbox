package eu.monniot.csc5002.emailbox.consumer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Consumer implements MessageListener {

    // Arguments
    private final String host;
    private final int port;
    private final String user;
    private final String password;
    private final String destination;
    private final Logger logger;
    private final Mode mode;


    private Connection connection;
    private Session session;
    private MessageConsumer consumer;

    public Consumer(String host, int port, String user, String password, String destination, Mode mode) throws JMSException {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.destination = destination;
        this.logger = LoggerFactory.getLogger(Consumer.class);
        this.mode = mode;

        setUp();
    }

    public void setUp() throws JMSException {
        logger.debug("Attempt to connect to ActiveMQ at tcp://{}:{}", host, port);
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);

        logger.debug("Create a connection with credential {}/{}", user, password);
        connection = factory.createConnection(user, password);
        connection.start();

        session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        Destination dst;
        if (mode == Mode.MODE_QUEUE) {
            dst = new ActiveMQQueue(destination);
            System.out.printf("Listening on mailbox `%s`.\n", destination);
        } else {
            dst = new ActiveMQTopic(destination);
            System.out.printf("Listening on newsgroup `%s`.\n", destination);
        }

        consumer = session.createConsumer(dst);
        consumer.setMessageListener(this);
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            String body = null;
            try {
                body = ((TextMessage) message).getText();
            } catch (JMSException e) {
                e.printStackTrace();
            }
            logger.info("Receive a new Message: {}", body);
        } else {
            logger.warn("Unexpected message type: {}", message.getClass());
        }

        try {
            message.acknowledge();
        } catch (JMSException e) {
            logger.warn("Message acknowledgement failed.");
            e.printStackTrace();
        }
    }

    public void tearDown() throws JMSException {
        consumer.close();
        session.close();
        connection.close();
    }

    public static void main(String[] args) throws JMSException {
        int port = Integer.parseInt(Helper.env("ACTIVEMQ_PORT", "61616"));
        String host = Helper.env("ACTIVEMQ_HOST", "localhost");
        String user = Helper.env("ACTIVEMQ_USER", "system");
        String password = Helper.env("ACTIVEMQ_PASSWORD", "manager");
        String destination = Helper.arg(args, 0, System.getProperty("user.name", "default"));

        List<Consumer> consumers = new ArrayList<>(1);
        consumers.add(new Consumer(host, port, user, password, destination, Mode.MODE_QUEUE));

        System.out.println("You can subscribe to a newsgroup by entering its name or enter q to exit.\n");
        while (true) {
            Scanner scanner = new Scanner(new InputStreamReader(System.in));

            String in = scanner.nextLine();
            if (in.equals("q")) {
                break;
            } else {
                consumers.add(new Consumer(host, port, user, password, in, Mode.MODE_TOPIC));
            }
        }

        for (Consumer consumer : consumers) {
            consumer.tearDown();
        }
    }

    private static enum Mode {
        MODE_QUEUE, MODE_TOPIC
    }
}
