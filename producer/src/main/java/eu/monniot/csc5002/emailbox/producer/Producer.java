package eu.monniot.csc5002.emailbox.producer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.UUID;

public class Producer {

    // Arguments
    private final String host;
    private final int port;
    private final String user;
    private final String password;
    private final String destination;
    private final Logger logger;
    private final String mode;

    // Created by this class
    private Session session;
    private MessageProducer producer;
    private Connection connection;

    public Producer(String host, int port, String user, String password, String destination, String mode) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.destination = destination;
        this.mode = mode;
        this.logger = LoggerFactory.getLogger(Producer.class);
    }

    public void setUp() throws JMSException {
        logger.info("Attempt to connect to ActiveMQ at tcp://{}:{}", host, port);
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);

        logger.info("Create a connection with credential {}/{}", user, password);
        connection = factory.createConnection(user, password);
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        logger.info("Create the queue {} (if didn't exist)", destination);
        Destination dst;
        if("newsgroup".equals(mode)) {
            dst = session.createTopic(destination);
        } else {
            dst = session.createQueue(destination);
        }
        producer = session.createProducer(dst);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }

    public void sendTimes(int messagesCount, String body) throws JMSException {
        for (int i = 1; i <= messagesCount; i++) {
            producer.send(session.createTextMessage(body));
        }
        logger.info("Sent {} message{}", messagesCount, (messagesCount > 1) ? "s" : "");
    }

    public void tearDown() throws JMSException {
        producer.close();
        session.close();
        connection.close();
    }

    public static void main(String[] args) throws JMSException {
        int port = Integer.parseInt(Helper.env("ACTIVEMQ_PORT", "61616"));
        String host = Helper.env("ACTIVEMQ_HOST", "localhost");
        String user = Helper.env("ACTIVEMQ_USER", "system");
        String password = Helper.env("ACTIVEMQ_PASSWORD", "manager");
        String destination = Helper.arg(args, 0, System.getProperty("user.name", "default"));
        String mode = Helper.arg(args, 1, "queue");

        Producer producer = new Producer(host, port, user, password, destination, mode);
        producer.setUp();
        producer.sendTimes(1, UUID.randomUUID().toString());
        producer.tearDown();
    }
}
